/**\mainpage   小算盤程式 Homework 7
 *  - \subpage 名稱：H7_110616038
 *  - \subpage 作者：110616038 陳泰元
 *  - \subpage 版本：1.0
 *  - \subpage 日期：2019/1/2
 *  
 *  操作說明：
 *  此程式為模仿微軟小算盤程式(標準型)
 *  顯示結果有兩列，上列顯示目前的運算步驟
 *  下列顯示目前累積的結果
 *  並且符合計算機不自動做運算子優先度的運算
 *  
 *  
 *  獨特功能：
 *  
 *  - \subpage 利用Doxygen編寫說明文件(110616038資料夾中)
 *  - \subpage 使用函式使主程式簡潔
 *  - \subpage 支援正負號轉換
 *  - \subpage 上列顯示列可以表現目前運算步驟
 *  - \subpage 下列顯示列可以隨時呈現目前運算結果
 *  - \subpage 除數為0時會通知使用者 
 *  - \subpage 增加清除鍵功能
 *  - \subpage 運算結果從小數變整數時會自動轉換為整數表現(反之亦同)
 *  
 *  符合的評分標準：
 *  - \subpage 程式有意義且可以執行 (+20%) 
 *  - \subpage 支援整數運算 (+30%) 
 *  - \subpage 支援小數運算(+25%)
 *  - \subpage 包含所有功能(+25%) 
 *  
 *  自評分數：
 *  99分（人總是無法盡善盡美 但是可以全力以赴！）
*/

import java.util.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;



public class H7_110616038 extends JFrame{
	
	private JFrame frm;
	private JPanel pan2;
	private JPanel pan3;
	private JButton btn0;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;
	private JButton btn7;
	private JButton btn8;
	private JButton btn9;
	private JButton btnDot;
	private JButton btnPlusMinus;
	private JButton btnClear;
	private JButton btnDivision;
	private JButton btnMul;
	private JButton btnPlus;
	private JButton btnMinus;
	private JButton btnEqual;
	
	private GridLayout grid1;
	private GridLayout grid2;
	
	private JPanel pan1;
	private JLabel lab1;
	private JLabel lab2;
	
	private Stack<Float> odStack;
	private Stack<String> orStack;
	
	private boolean flagNum;
	private boolean flagOr; 
	private boolean flagDec;
	private boolean flagEqual;

	
	/**
	 * @brief 建構元
	 */
	public H7_110616038() {
		
		odStack = new Stack<Float>();
		orStack = new Stack<String>();
		
		flagNum = false; //紀錄是否有更新數字
		flagOr = true; //更新二元運算子滿足狀況
		flagDec = false; //是否進入小數模式
		flagEqual = false; //是否為等於模式
		
		//基本視窗建置
		frm = new JFrame("小算盤");
		grid1 = new GridLayout(1,2,10,10);
		grid2 = new	GridLayout(5,5,10,10);
		pan1 = new JPanel();
		lab1 = new JLabel("",JLabel.RIGHT);
		lab1.setFont(new Font("Microsoft JhengHei UI Light", Font.PLAIN, 16));
		lab2 = new JLabel("0",JLabel.RIGHT);
		lab2.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 50));
		pan2 = new JPanel();
		pan3 = new JPanel();
		
		frm.setSize(345, 500);
		frm.setLocationRelativeTo(null);
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setResizable(false);
		frm.getContentPane().setLayout(null);
		
		
		pan1.setBounds(10, 0, 319, 122);
		pan1.setLayout(null);
		frm.getContentPane().add(pan1);
		
		
		
		lab1.setBounds(0, 22, 309, 32);
		pan1.add(lab1);
		
		
		lab2.setBounds(0, 64, 309, 58);
		pan1.add(lab2);
		
		
		pan2.setBounds(171, 132, 158, 63);
		pan2.setLayout(new GridLayout(1, 2, 5, 5));
		frm.getContentPane().add(pan2);
		
		
		btnClear = new JButton("C");
		btnClear.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btnClear.addActionListener(new btnNumber());
		pan2.add(btnClear);
		
		btnDivision = new JButton("÷");
		btnDivision.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btnDivision.addActionListener(new btnNumber());
		pan2.add(btnDivision);
		
		
		pan3.setBounds(10, 205, 319, 256);
		pan3.setLayout(new GridLayout(4, 4, 5, 5));
		frm.getContentPane().add(pan3);
		
		
		btn7 = new JButton("7");
		btn7.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn7.addActionListener(new btnNumber());
		pan3.add(btn7);
		
		btn8 = new JButton("8");
		btn8.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn8.addActionListener(new btnNumber());
		pan3.add(btn8);
		
		btn9 = new JButton("9");
		btn9.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn9.addActionListener(new btnNumber());
		pan3.add(btn9);
		
		btnMul = new JButton("×");
		btnMul.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btnMul.addActionListener(new btnNumber());
		pan3.add(btnMul);
		
		btn4 = new JButton("4");
		btn4.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn4.addActionListener(new btnNumber());
		pan3.add(btn4);
		
		btn5 = new JButton("5");
		btn5.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn5.addActionListener(new btnNumber());
		pan3.add(btn5);
		
		btn6 = new JButton("6");
		btn6.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn6.addActionListener(new btnNumber());
		pan3.add(btn6);
		
		btnPlus = new JButton("+");
		btnPlus.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btnPlus.addActionListener(new btnNumber());
		pan3.add(btnPlus);
		
		btn1 = new JButton("1");
		btn1.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn1.addActionListener(new btnNumber());
		pan3.add(btn1);
		
		btn2 = new JButton("2");
		btn2.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn2.addActionListener(new btnNumber());
		pan3.add(btn2);
		
		btn3 = new JButton("3");
		btn3.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn3.addActionListener(new btnNumber());
		pan3.add(btn3);
		
		btnMinus = new JButton("－");
		btnMinus.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btnMinus.addActionListener(new btnNumber());
		pan3.add(btnMinus);
		
		btnPlusMinus = new JButton("±");
		btnPlusMinus.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btnPlusMinus.addActionListener(new btnNumber());
		pan3.add(btnPlusMinus);
		
		btn0 = new JButton("0");
		btn0.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btn0.addActionListener(new btnNumber());
		pan3.add(btn0);
		
		btnDot = new JButton(".");
		btnDot.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btnDot.addActionListener(new btnNumber());
		pan3.add(btnDot);
		
		btnEqual = new JButton("=");
		btnEqual.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 24));
		btnEqual.addActionListener(new btnNumber());
		pan3.add(btnEqual);
		

		frm.setVisible(true);
	}
	
	/**
	 * @brief 主程式
	 */
	public static void main(String[] args) {
		H7_110616038 Window = new H7_110616038();
	}
	
	/**
	 * @brief 實作ActionListener當聆聽者
	*/
	class btnNumber implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() instanceof JButton) {  //判別事件來源者
				JButton btnIn = (JButton) e.getSource();
				if(btnIn == btn0) {
					if(lab2.getText()!="0" && flagNum != false) //判別0的輸入以及條件
						lab2.setText(lab2.getText()+"0");
					if(flagNum == false) { //可以直接為0的狀況
						lab2.setText("0");
					}
					flagNum = true;
				}
				if(btnIn == btn1) {
					if(lab2.getText()!="0" && flagNum != false)
						lab2.setText(lab2.getText()+"1");
					else
						lab2.setText("1");
					flagNum = true;
				}
				if(btnIn == btn2) { 
					if(lab2.getText()!="0" && flagNum != false)
						lab2.setText(lab2.getText()+"2");
					else
						lab2.setText("2");
					flagNum = true;
				}
				if(btnIn == btn3) {
					if(lab2.getText()!="0" && flagNum != false)
						lab2.setText(lab2.getText()+"3");
					else
						lab2.setText("3");
					flagNum = true;
				}
				if(btnIn == btn4) {
					if(lab2.getText()!="0" && flagNum != false)
						lab2.setText(lab2.getText()+"4");
					else
						lab2.setText("4");
					flagNum = true;
				}
				if(btnIn == btn5) {
					if(lab2.getText()!="0" && flagNum != false)
						lab2.setText(lab2.getText()+"5");
					else
						lab2.setText("5");
					flagNum = true;
				}
				if(btnIn == btn6) {
					if(lab2.getText()!="0" && flagNum != false)
						lab2.setText(lab2.getText()+"6");
					else
						lab2.setText("6");
					flagNum = true;
				}
				if(btnIn == btn7) {
					if(lab2.getText()!="0" && flagNum != false)
						lab2.setText(lab2.getText()+"7");
					else
						lab2.setText("7");
					flagNum = true;
				}
				if(btnIn == btn8) {
					if(lab2.getText()!="0" && flagNum != false)
						lab2.setText(lab2.getText()+"8");
					else
						lab2.setText("8");
					flagNum = true;
				}
				if(btnIn == btn9) {
					if(lab2.getText()!="0" && flagNum != false)
						lab2.setText(lab2.getText()+"9");
					else 
						lab2.setText("9");
					flagNum = true;
				}
				//功能鍵會呼叫函式
				if(btnIn == btnPlus) {  //加法
					plusProcess();
				}
				if(btnIn == btnMinus) { //減法
					minusProcess();
				}
				if(btnIn == btnMul) { //乘法
					mulProcess();
				}
				if(btnIn == btnDivision) { //除法
					divisionProcess();
				}
				if(btnIn == btnPlusMinus) { //取正負
					absProcess();
				}
				if(btnIn == btnClear) { //清除
					clearProcess();
				}
				if(btnIn == btnDot) { //小數點
					decProcess();
				}
				if(btnIn == btnEqual) { //等於
					equalProcess();
				}
			}
		}
	}
	
	/**
	 * @brief 加法運算
	 */
	private void plusProcess() {
		
		StringBuffer temp = new StringBuffer(lab1.getText());
		float A = 0f;
		
		if(flagNum||flagEqual) { //判別可以進入的條件(按下等於或是已經有數字更新)
			temp.insert(temp.length(), lab2.getText());
			
			lab1.setText(temp.toString()+"+");
			A = Float.parseFloat(lab2.getText());
			odStack.push((float)A);
			//改變旗標狀態
			flagNum = false;
			flagOr = false;
			flagEqual = false;
			opeProcess("+"); //呼叫處理程序

		}else {
			if(temp.length() != 0 && flagOr == true) {
				temp.insert((temp.length()), "+");
				lab1.setText(temp.toString());
				flagOr = false;
			}
		}
	}
	
	/**
	 * @brief 減法運算
	 */
	private void minusProcess() {
		
		StringBuffer temp = new StringBuffer(lab1.getText());
		float A = 0f;
		
		if(flagNum||flagEqual) { //判別可以進入的條件(按下等於或是已經有數字更新)
	
			temp.insert(temp.length(), lab2.getText());

			lab1.setText(temp.toString()+"－");
			A = Float.parseFloat(lab2.getText());
			odStack.push((float)A);
			//改變旗標狀態
			flagNum = false;
			flagOr = false;
			flagEqual = false;
			opeProcess("-"); //呼叫處理程序

		}else {
			if(temp.length() != 0 && flagOr == true) {
				temp.insert((temp.length()), "－");
				lab1.setText(temp.toString());
				flagOr = false;
			}
		}
	}
	
	/**
	 * @brief 乘法運算
	 */
	private void mulProcess() {
		
		StringBuffer temp = new StringBuffer(lab1.getText());
		float A = 0f;
		
		if(flagNum||flagEqual) { //判別可以進入的條件(按下等於或是已經有數字更新)
	
			temp.insert(temp.length(), lab2.getText());

			lab1.setText(temp.toString()+"×");
			A = Float.parseFloat(lab2.getText());
			//改變旗標狀態
			odStack.push((float)A);
			flagNum = false;
			flagOr = false;
			flagEqual = false;
			opeProcess("*"); //呼叫處理程序

		}else {
			if(temp.length() != 0 && flagOr == true) {
				temp.insert((temp.length()), "×");
				lab1.setText(temp.toString());
				flagOr = false;
			}
		}
	}
	
	/**
	 * @brief 除法
	 */
	private void divisionProcess() {
		
		StringBuffer temp = new StringBuffer(lab1.getText());
		float A = 0f;
		
		if(flagNum||flagEqual) { //判別可以進入的條件(按下等於或是已經有數字更新)
	
			temp.insert(temp.length(), lab2.getText());

			lab1.setText(temp.toString()+"÷");
			A = Float.parseFloat(lab2.getText());
			odStack.push((float)A);
			//調整旗標狀態
			flagNum = false;
			flagOr = false;
			flagEqual = false;
			opeProcess("/"); //呼叫處理程序

		}else {
			if(temp.length() != 0 && flagOr == true) {
				temp.insert((temp.length()),"÷");
				lab1.setText(temp.toString());
				flagOr = false;
			}
		}
	}
	
	/**
	 * @brief 正負轉換功能
	 */
	private void absProcess() {
		float temp = 0f;
		
		temp = Float.parseFloat(lab2.getText());
		temp = -temp; //與目前正負相反
		
		if(flagDec) { //判別現在是否為小數模式
			lab2.setText(Float.toString(temp));
		}else {
			lab2.setText(Integer.toString((int)temp));
		}
	}
	
	/**
	 * @brief 清除功能
	 */
	private void clearProcess() {
		//狀態初始化
		flagNum = false;
		flagOr = true;
		flagDec = false;
		
		//stack初始化
		while(!odStack.isEmpty()){
			odStack.pop();
		}
		while(!orStack.isEmpty()){
			orStack.pop();
		}
		
		//顯示列初始化
		lab1.setText("");
		lab2.setText("0");
	}
	
	/**
	 * @brief 小數模式
	 */
	private void decProcess() {
		StringBuffer temp = new StringBuffer(lab2.getText());	
		
		flagDec = true; //開起小數狀態旗標
		temp.insert((temp.length()),".");
		lab2.setText(temp.toString());
	}
	
	/**
	 * @brief 等於功能
	 */
	private void equalProcess() {
		Float temp = 0f;
		
		if(!orStack.isEmpty()) { //判別stack的狀況
			temp = Float.parseFloat(lab2.getText());
			odStack.push(temp);
			//調整旗標狀態
			flagEqual = true;
			flagNum = false;
			//呼叫運算程序
			opeProcess(orStack.peek());
			lab1.setText("");
		}
	}

	/**
	 * @brief 幕後運算程序
	 */
	private void opeProcess(String operator) {
		
		float B = 0f ;
		float C = 0f ;
		float D = 0f;
		String tempOperator;
		boolean flagNoDivi = false; //判別是否除數為零
		
		if(orStack.isEmpty()) {
			orStack.push(operator);
		}else {
			tempOperator = orStack.pop();
			B = odStack.pop();
			C = odStack.pop();
			//判別呼叫者的運算子為何來決定處理內容
			switch(tempOperator) {
			case "+":
				D = C+B;
				break;
			case "-":
				D =(C-B);
				break;
			case "*":
				D = C*B;
				break;
			case "/":
				if(B == 0) { //除數為0
					flagNoDivi = true;
				}else {
					D = C/B;
				}
				
				if((C%B)==0) { //判斷是否要開啟小數模式
					flagDec = false;
				}else {
					flagDec = true;
				}
				break;
			}
			
			if((int)D == D) { //判斷是否要結束小數模式
				flagDec = false;
			}
			
			
			if(flagNoDivi) { //顯示狀況
				lab2.setText("無法除以0");
				flagNum = false; //需要再次輸入其他數字
			}else {
				if(flagDec) { //判別是否為小數模式
					lab2.setText((Float.toString(D)));
				}else {
					lab2.setText((Integer.toString((int)D)));
				}
				if(!flagEqual) { //是否為等於模式呼叫的
					odStack.push((float)D);
					orStack.push(operator);
				}
			}

		}
	}

}
